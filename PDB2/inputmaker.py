# -*- coding: utf-8 -*-
"""
Created on Fri Nov  7 13:21:25 2014

@author: erikthiede

A collection of scripts used for running Midway Files en masse.
"""

import re
import numpy as np
import sys

def replace_copy(template_name, outfile_name, replacement_strings):
    """
    Copies the template file to out_file_name, replacing any string the user
    wishes to be replaced.  Takes as input..
    """
    template = open(template_name)
    outfile = open(outfile_name, 'w+')

    for line in template:
        for tempstring, replacement in list(replacement_strings.items()):
            line = re.sub(tempstring, replacement, line)
        outfile.write(line)
    outfile.close()
    template.close()


def makeInputs(template_name,outfile_name):
    """
    Writes the input for an umbrella sampling calculation.
    """
    template = open(template_name)
    outfile = open(outfile_name, 'w+')
    for line in template:
        randint=str(np.random.randint(0,446))
        line = re.sub('RANDOM', randint, line)
        line = re.sub('OUT', sys.argv[1], line)

        outfile.write(line)
    outfile.close()
    template.close()


def minimage(rv, period):
    return rv - period * np.rint(rv/period)


def main():
    rank=int(sys.argv[1])
    inputfile='mixture-comment.inp'
    outputfile='mixture-comment%d.inp'%rank
    makeInputs(inputfile,outputfile)


# Standard boiler plate that makes sure that the main function runs only if
# this program is run directly.  Basically, it allows this program to be used
# as a resource for other programs without fudging things up.

if __name__ == '__main__':
    main()
