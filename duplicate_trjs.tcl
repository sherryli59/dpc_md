### Use in VMD ###
### Function: generate a system filled with randomly positioned and oriented molecules with various conformations
### Input:  nmol - number of molecules
###         fname - prefix of the pdbfile as well as the output file
###         boxlen - length of simulation box
###         dist - minimum distance allowed between the inserted molecules
###         trjfile - trajectory file containing different conformations of the inserted molecule
###                   USE gmx trjconv to make sure that the structures are intact in the trajectory file
###                   For example: gmx trjconv -f nvt.xtc -s nvt.tpr -pbc mol -o mol_test.xtc
###                   @gpu30: /home/weng/PhePhe-PhgPhg/1FF-CG22
### Output: outfile in PDB format
### by jwweng 2020.9.14
###
### For example: @gpu30:/home/weng/PhePhe-PhgPhg/1FF-CG21/FF-CG2154_8.0_1000/
###              @gpu30:/home/weng/PhePhe-PhgPhg/1FF-CG21/


### options ###
set nmol    54
set fname   DPC_charmm
set boxlen  100.0
set dist    8.0
set trjfile trajout.xtc
set initIdx 0
set lastIdx 4

### cycle ###
for {set ich $initIdx} {$ich <= $lastIdx} {incr ich} {
  set uch _$ich
  set outpdb  ${fname}${nmol}_${dist}${uch}.pdb
  
  ### parameters ###
  set solopdb ${fname}.pdb
  set oripdb  ${fname}${nmol}_${dist}_ori.pdb
  
  ### 1a.get original snapshots from trajectory file ###
  set tmppdb tmp.pdb
  set molid [mol new $solopdb]
  set sel [atomselect $molid all]
  animate delete beg 0 end 0 $molid
  
  #### 1b. get resid list ###
  set resilst [lsort -unique [$sel get resid]]
  set nres [llength $resilst]
  foreach u $resilst {
    set natm_in_res($u) [[atomselect $molid "resid $u"] num]
  }
  
  set count_res 1
  set full_reslst ""
  for {set imol 0} {$imol < $nmol} {incr imol} {
    foreach ures $resilst {
      for {set iatm 0} {$iatm < $natm_in_res($ures)} {incr iatm} {
        lappend full_reslst $count_res
      }
      incr count_res
    }
  }
  
  ### 2.get initial PDBs.pdb ###
  mol addfile $trjfile waitfor all
  set nframe [molinfo $molid get numframes]
  
  for {set iframe 0} {$iframe < $nframe} {incr iframe} {
    $sel frame $iframe
    $sel moveby [vecinvert [measure center $sel weight mass]]
  }
  
  set wfl [open $oripdb w]
  puts $wfl "CRYST1[format %9.3f $boxlen][format %9.3f $boxlen][format %9.3f $boxlen]  90.00  90.00  90.00 P 1           1"
  for {set i 1} {$i <= $nmol} {incr i} {
    $sel set resid $i
    set iframe [expr int(rand() * $nframe)]
    puts "frame $iframe selected"
    animate write pdb $tmppdb beg $iframe end $iframe $molid
    set rfl [open $tmppdb r]
    gets $rfl line
    while {[gets $rfl line] >= 0} {
      if {$line != "END"} {
        puts $wfl $line
      }
    }
    close $rfl
  }
  close $wfl
  
  #debug gets stdin
  ### 3.get original many residues PDB file ###
  set molid [mol new $oripdb]
  for {set i 1} {$i <= $nmol} {incr i} {
    set sel    [atomselect $molid "resid $i"]
    $sel moveby [vecinvert [measure center $sel weight mass]]
    $sel move [transaxis x [expr rand()*360.] deg]
    $sel move [transaxis y [expr rand()*360.] deg]
    $sel move [transaxis z [expr rand()*360.] deg]
    set isrep 1
    while {$isrep == 1} {
      set isrep 0
      set movx [expr $boxlen * rand()]
      set movy [expr $boxlen * rand()]
      set movz [expr $boxlen * rand()]
      $sel moveby "$movx $movy $movz"
      for {set ix -1} {$ix <= 1} {incr ix} {
        for {set iy -1} {$iy <= 1} {incr iy} {
          for {set iz -1} {$iz <= 1} {incr iz} {
            $sel moveby "[expr $boxlen * $ix] [expr $boxlen * $iy] [expr $boxlen * $iz]"
            set  selnot [atomselect $molid "resid $i and (within $dist of (not resid $i))"]
            if {[$selnot num] != 0} {
              set isrep 1
  #            puts [$selnot num]
            }
            $selnot delete
            $sel moveby "[expr -$boxlen * $ix] [expr -$boxlen * $iy] [expr -$boxlen * $iz]"
          }
        }
      }
      $sel moveby "-$movx -$movy -$movz"
    }
    $sel moveby "$movx $movy $movz"
    $sel delete
    puts $i
  }
  
  [atomselect $molid all] set resid $full_reslst
  animate write pdb $outpdb beg 0 end 0 $molid

}



